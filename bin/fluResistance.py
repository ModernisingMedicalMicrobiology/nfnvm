#! /usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
import gzip
import os
import subprocess
import numpy as np
import re


def dictFluResistTable(fluResistTable):
	dictFluResist = []
	
	fluResistTable_file = open(fluResistTable, 'r')
	fluResistTable_file.readline()

	for line in fluResistTable_file:
		line = line.rstrip('\n')
		element = line.split("\t")
		dictFluResist.append(element)
		dictFluResistM = np.matrix(dictFluResist)
	
	fluResistTable_file.close()
	return dictFluResistM
    
def detectFluResist(seqFile, splID, dictFluResistM):
    fofn=''
    headline = "\t".join(("SampleID", "FluAsubtype", "Gene", "Mutation", "AntiviralDrug", "AntiviralType"))
    fofn += '{0}\n'.format(headline)
    mutation = "no mutation" 
    
    for seq in SeqIO.parse(seqFile,'fasta'):
        seqID = seq.id
        seqIDT = seqID.split("_")
        
        if seqIDT[-1]=="M2":      
            fluType = "fluA"
            fluGene = "M2"
        if seqIDT[-1]=="NA" and seqIDT[-2]=="H1N1":
            fluType = "AH1N1pdm09"
            fluGene = "NA"
        if seqIDT[-1]=="NA" and seqIDT[-2]=="H3N2":
            fluType = "AH3N2"
            fluGene = "NA"

        for m in range(dictFluResistM.shape[0]):
            if re.search("\+", dictFluResistM[m,2]):
                coMutation = dictFluResistM[m,2].split("+")
                resistAA_1 = coMutation[0][-1:]
                resistAA_2 = coMutation[1][-1:]

                resistPos_1 = coMutation[0][:-1]
                resistPos_1 = resistPos_1[1:]
                resistPos_2 = coMutation[1][:-1]
                resistPos_2 = resistPos_2[1:]
			
                if fluType == dictFluResistM[m,0] and fluGene == dictFluResistM[m,1] and seq.seq[int(resistPos_1)-1] == resistAA_1 and seq.seq[int(resistPos_2)-1] == resistAA_2:
                    fofn += '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(seqID.split(".")[0], seqIDT[-2], seqIDT[-1], dictFluResistM[m,2], dictFluResistM[m,3], dictFluResistM[m,4])
                    mutation = "mutation found"
					
            elif re.search("\(", dictFluResistM[m,2]):
                pass

            else:
                resistAA = dictFluResistM[m,2][-1:]				
                resistPos = dictFluResistM[m,2][:-1]
                resistPos = resistPos[1:]
                
                if fluType == dictFluResistM[m,0] and fluGene == dictFluResistM[m,1] and seq.seq[int(resistPos)-1] == resistAA:
                    fofn += '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(seqID.split(".")[0], seqIDT[-2], seqIDT[-1], dictFluResistM[m,2], dictFluResistM[m,3], dictFluResistM[m,4])
                    mutation = "mutation found"
                    
        if mutation == "no mutation":
            fofn += '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(seqID.split(".")[0], seqIDT[-2], seqIDT[-1], "Not detected", "", "")

        if fluType in ["AH3N2", "AH1N1pdm09", "fluA"]:
            with open("{0}.fluResistance".format(splID),'wt') as outhandle:
                outhandle.write(fofn)

    return fofn


if __name__ == "__main__":
    parser = ArgumentParser(description='detect flu antiviral resistance mutations')
    parser.add_argument('-seqFile','--seqFile', required=True, help='fasta sequence file')
    parser.add_argument('-splID','--splID', required=True, help='sample ID')
    parser.add_argument('-fluResistTable','--fluResistTable', required=True, help='table list influenza drug resistance mutations')
    
    opts, unknown_args = parser.parse_known_args()
    d = dictFluResistTable(opts.fluResistTable)
    f = detectFluResist(opts.seqFile, opts.splID, d)
