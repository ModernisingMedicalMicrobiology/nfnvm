#! /usr/bin/env python3
import argparse
import os


def consensus(inFile, splID, depCov, pctCall):
    depCov = int(depCov)
    pctCall = float(pctCall)

    inf_out = open("{0}_voteConsensus.fasta".format(splID), 'w')
    
    inf_in = open(inFile, 'r')
    inf_in.readline()
    refNum_ori = ""
    tblLine = 0
    
    for line in inf_in:
        tblLine = tblLine + 1
        line = line.rstrip()
        temp = line.split("\t")
        refNum = temp[0]

        if refNum != refNum_ori and refNum_ori == "":
            sequence = ""
            refNum_ori = refNum

        elif refNum != refNum_ori and refNum_ori != "":
            inf_out.write("{0}{1}\n".format(">",refNum_ori))
            inf_out.write("{0}\n".format(sequence))
            sequence = ""
            refNum_ori = refNum

        depth = int(temp[3])

        A = int(temp[14])
        C = int(temp[16])
        T = int(temp[20])
        G = int(temp[18])

        Apct = temp[15]
        Cpct = temp[17]
        Tpct = temp[21]
        Gpct = temp[19]

        votes = [A, C, T, G]
        votes_pct = [Apct, Cpct, Tpct, Gpct]


        # 6 - '-' : depth of coverage is less than a cutoff value
        # 5 - 'N' : more than 1 nucleotide has the same number of vote
        # 4 - 'N' : major vote is supported by less than a certain percentage of reads
        # 3 - 'G' : G is supported by more than a certain percentage of reads
        # 2 - 'T' : T is supported by more than a certain percentage of reads
        # 1 - 'C' : C is supported by more than a certain percentage of reads
        # 0 - 'A' : A is supported by more than a certain percentage of reads
        
        if depth < depCov:
            case = 6

        elif votes.count(max(votes)) > 1:
            case = 5

        else:
            if float(votes_pct[votes.index(max(votes))]) >= pctCall:
                case = votes.index(max(votes))
            else:
                case = 4

        if case == 0:
            nucl = "A"
        elif case == 1:
            nucl = "C"
        elif case == 2:
            nucl = "T"
        elif case == 3:
            nucl = "G"
        elif case == 4:
            nucl = "N"
        elif case == 5:
            nucl = "N"
        elif case == 6:
            nucl = "-"

        sequence=sequence+nucl

    inf_out.write("{0}{1}\n".format(">",refNum_ori))
    inf_out.write("{0}\n".format(sequence))
            
    inf_in.close()
    inf_out.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Produce draft consensus sequences from mapping profile')
    
    parser.add_argument("-inFile", dest="inFile", required= True, help = "Updated mapping profiles")
    parser.add_argument("-splID", dest="splID", required= True, help = "sample ID")
    parser.add_argument("-depCov", dest="depCov", required= True, help = "minimal depth of coverage for a position to be considered")
    parser.add_argument("-pctCall", dest="pctCall", required= True, help = "Proportion of reads support the genotype call at each position")

    params = parser.parse_args()
    
    consensus(inFile = params.inFile, splID = params.splID, depCov = params.depCov, pctCall = params.pctCall)
