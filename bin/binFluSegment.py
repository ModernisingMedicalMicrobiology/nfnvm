#! /usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
import gzip
import os
import subprocess


def dictFluTable(fluTable):
    dict = {}

    fluTable_file = open(fluTable, 'r')
    for line in fluTable_file:
        line = line.rstrip()
        temp = line.split("\t")

        seqAcc = temp[0]
        lineB = "{0}\t{1}".format(temp[2], temp[3])
        dict.setdefault(seqAcc, lineB)

    fluTable_file.close()
    return dict

def fofnCent(Cent, dict):
    fofn=''
    headline = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format("readID", "seqAcc", "taxID", "Class", "Species", "fluSubtype")
    fofn+='{0}\n'.format(headline)

    centResult_file = open(Cent, 'r')
    centResult_file.readline()
    for line in centResult_file:
        line = line.rstrip()
        temp = line.split("\t")
        tempC = temp[1].split("\.")
        readID = tempC[0]

        if readID in dict:
            fluInfo = dict[readID].split("\t")
            fofn+='{0}\t{1}\n'.format(line+fluInfo[0], fluInfo[1])
        else:
            fofn+='{0}\t{1}\n'.format(line, "undetermined")

    centResult_file.close()
    return fofn

def writeFofn(splID, fofn):
    with open("{0}_classFlu.Report".format(splID),'wt') as outhandle:
        outhandle.write(fofn)


if __name__ == "__main__":
    parser = ArgumentParser(description='bin reads into 8 influenza gene segments')
    parser.add_argument('-fluTable','--fluTable', required=True, help='Table of flu sequences in centrifuge database')
    parser.add_argument('-Cent','--Cent', required=True, help='Centrifuge results table')
    parser.add_argument('-splID','--splID', required=True, help='sample ID')

    opts, unknown_args = parser.parse_known_args()
    dict = dictFluTable(opts.fluTable)
    f = fofnCent(opts.Cent, dict)
    writeFofn(opts.splID, f)
