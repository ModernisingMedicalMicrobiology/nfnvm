#!/usr/bin/env python
from Bio import SeqIO
import sys
import vcf
import subprocess
from collections import defaultdict
import os.path
import operator


reference = sys.argv[1]
vcffile = sys.argv[2]
bamfile = sys.argv[3]
varcaller = sys.argv[4]

#DEPTH_THRESHOLD = 20
DEPTH_THRESHOLD = 10


def collect_depths(bamfile):
    if not os.path.exists(bamfile):
        raise SystemExit("bamfile %s doesn't exist" % (bamfile,))

    print >>sys.stderr, bamfile

    p = subprocess.Popen(['samtools', 'depth', bamfile],
                             stdout=subprocess.PIPE)
    out, err = p.communicate()
    depths = defaultdict(dict)
    for ln in out.split("\n"):
            if ln:
                    contig, pos, depth = ln.split("\t")
                    depths[contig][int(pos)] = int(depth)
    return depths

depths = collect_depths(bamfile)

def report(r, status, allele):
    idfile = os.path.basename(vcffile).split(".")[0]
    print >>sys.stderr, "%s\t%s\tstatus\t%s" % (idfile, r.POS, status)
    print >>sys.stderr, "%s\t%s\tdepth\t%s" % (idfile, r.POS, record.INFO.get('TotalReads', ['n/a']))
    print >>sys.stderr, "%s\t%s\tbaseFrac\t%s" % (idfile, r.POS, record.INFO.get('BaseCalledFraction', ['n/a']))
    print >>sys.stderr, "%s\t%s\tallele\t%s" % (idfile, r.POS, allele)
    print >>sys.stderr, "%s\t%s\tref\t%s" % (idfile, r.POS, record.REF)
    print >>sys.stderr, "%s\t%s\tsupportFrac\t%s" % (idfile, r.POS, record.INFO.get('SupportFraction', ['n/a']))


cons = ''

seq = list(SeqIO.parse(open(sys.argv[1]), "fasta"))[0]
cons = list(seq.seq)

for n, c in enumerate(cons):
    try:
        depth = depths[seq.id][n+1]
    except:
        depth = 0

    if depth < DEPTH_THRESHOLD:
        cons[n] = 'N'


sett = set()
vcf_reader = vcf.Reader(open(vcffile, 'r'))
for record in vcf_reader:
	if record.ALT[0] != '.':
        # variant call
		
		if varcaller == "nanopolish":
			support = float(record.INFO['SupportFraction'])
			total_reads = int(record.INFO['TotalReads'])
			qual = record.QUAL

			#if qual >= 200 and total_reads >= 50:
			#if support >= 0.75 and total_reads > 20:
			if support >= 0.75 and total_reads >= 10:
				ALT = record.ALT[0]
				report(record, "variant", ALT)

				sett.add(record.POS)
				cons[record.POS-1] = str(ALT)
			
			else:
				report(record, "low_qual_variant", "n")
				cons[record.POS-1] = 'N'
				continue    

		elif varcaller == "clair":
			qual = record.QUAL

			if qual > 750:
				ALT = record.ALT[0]
				report(record, "variant", ALT)

				sett.add(record.POS)
				cons[record.POS-1] = str(ALT)
			
			else:
				report(record, "low_qual_variant", "n")
				cons[record.POS-1] = 'N'
				continue
		
		elif varcaller == "medaka":
			qual = record.QUAL

			if qual > 30:
				ALT = record.ALT[0]
				report(record, "variant", ALT)

				sett.add(record.POS)
				cons[record.POS-1] = str(ALT)
			
			else:
				report(record, "low_qual_variant", "n")
				cons[record.POS-1] = 'N'
				continue

#print >>sys.stderr, str(sett)
print ">%s" % (sys.argv[3])
print "".join(cons)