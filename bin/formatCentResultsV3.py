#! /usr/bin/env python3

import argparse

def make_dicts(table_text):
    taxid_class_dict = dict()
    taxid_species_dict = dict()

    for line in table_text.split('\n')[1:]:
        try:
            taxid, c, s = line.split('\t')
        except:
            print(line)
            continue
        taxid_class_dict[taxid] = c
        taxid_species_dict[taxid] = s
    
    return taxid_class_dict, taxid_species_dict

def formatCent(inFile, taxid_species_dict, taxid_class_dict):
	fofn=''
	headline = "\t".join(( "readID", "seqAcc", "taxID", "Class", "Species"))
	fofn += '{0}\n'.format(headline)
	#fofn += f"{headline}\n"
	
	with open(inFile, 'r') as centResult_file:
		next(centResult_file)
		for line in centResult_file:
			line = line.rstrip()
			temp = line.split("\t")
		
			readID = temp[0]
			sseqID = temp[1]
			queryTaxID = temp[2]
			centScore = temp[3]
			if centScore.isdigit():
				centScore = int(centScore)
				if centScore >= 150:
					if queryTaxID in taxid_class_dict:
						fofn += "{0}\t{1}\t{2}\t{3}\t{4}\n".format(readID, sseqID, queryTaxID, taxid_class_dict[queryTaxID], taxid_species_dict[queryTaxID])
					else:
						fofn += "{0}\t{1}\t{2}\t{3}\t{4}\n".format(readID, sseqID, queryTaxID, "others", "undetermined")
				else:
					fofn += "{0}\t{1}\t{2}\t{3}\t{4}\n".format(readID, "undetermined", "undetermined", "undetermined", "undetermined")
	return fofn

def writeFofn(splID, fofn):
    #with open(f"{splID}_Cent.resultsFormat", 'w') as outhandle:
	with open("{0}_Cent.resultsFormat".format(splID), 'w') as outhandle:
		outhandle.write(fofn)

def main():
    parser = argparse.ArgumentParser(description='parese the viral reads ID')
    parser.add_argument("-inFile", dest="inFile", required= True, help = "Centrifuge results table")
    parser.add_argument("-splID", dest="splID", required= True, help = "sample ID")
    parser.add_argument("-table", dest="table", required= True, help = "TaxID table for common species in respiratory samples")
    opts, unknown_args = parser.parse_known_args()
    taxid_class_dict, taxid_species_dict = make_dicts(open(opts.table).read())
    f = formatCent(opts.inFile, taxid_species_dict, taxid_class_dict)
    writeFofn(opts.splID, f)

if __name__ == '__main__':
    main()
