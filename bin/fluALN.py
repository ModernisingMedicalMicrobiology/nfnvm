#! /usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
import gzip
import os
import subprocess
import numpy as np
import re


def parseBLAST(blastFile, splID):
    numAln = 0
    with open(blastFile, 'r') as BLAST:
        for line in BLAST:
            line = line.strip()
            if line.startswith("<Hsp_qseq>") and line.endswith("</Hsp_qseq>"):
                alignment = line.replace("<Hsp_qseq>", "")
                alignment = alignment.replace("</Hsp_qseq>", "")
            elif line.startswith("<Hit_id>") and line.endswith("</Hit_id>"):
                subjectName = line.replace("<Hit_id>", "")
                subjectName = subjectName.replace("</Hit_id>", "")
            elif line.startswith("<Hsp_hit-from>") and line.endswith("</Hsp_hit-from>"):
                subjectStartPos = line.replace("<Hsp_hit-from>", "")
                subjectStartPos = int(subjectStartPos.replace("</Hsp_hit-from>", ""))
            elif line.startswith("<Hit_num>") and line.endswith("</Hit_num>"):
                numAln = line.replace("<Hit_num>", "")
                numAln = int(numAln.replace("</Hit_num>", ""))
            else:
                pass
    
    if numAln == 1:
        if subjectStartPos == 1:
            proteinSeq = alignment
        elif subjectStartPos >1:
            proteinSeq = "-"*(subjectStartPos-1) + alignment

        outSeq = open("{0}_protein.fasta".format(splID), 'w')    
        outSeq.write("{0}{1}_{2}\n".format(">", splID, subjectName))
        outSeq.write("{0}\n".format(proteinSeq))
        outSeq.close()

    else:
        outSeq = open("{0}_protein.fasta".format(splID), 'w')
        outSeq.close()


if __name__ == "__main__":
    parser = ArgumentParser(description='detect flu antiviral resistance mutations')
    parser.add_argument('-blastFile','--blastFile', required=True, help='blast report file')
    parser.add_argument('-splID','--splID', required=True, help='sample ID')
    
    opts, unknown_args = parser.parse_known_args()
    parseBLAST(opts.blastFile, opts.splID)
    