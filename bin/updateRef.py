#! /usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
import gzip
import os
import subprocess


def BRtable(BlastTable):
    dict = {}

    BTable_file = open(BlastTable, 'r')
    for line in BTable_file:
        line = line.rstrip()
        temp = line.split("\t")

        qAcc = temp[0]
        sAcc = temp[1]
        blastIden = float(temp[2])
        if blastIden >= 85:
            dict.setdefault(qAcc, sAcc)

    BTable_file.close()
    return dict

def fofnRef(ref, dict):
    fofn=''
    headline = "{0}\t{1}\t{2}".format("Acc", "Pathogen", "Acc2")
    fofn+='{0}\n'.format(headline)

    ref_file = open(ref, 'r')
    ref_file.readline()
    for line in ref_file:
        line = line.rstrip()
        acc = line.split("\t")[0]

        if acc in dict:
            fofn+='{0}\t{1}\n'.format(line, dict[acc])

        else:
            fofn+='{0}\t{1}\n'.format(line, acc)

    ref_file.close()
    return fofn

def writeFofn(splID, fofn):
    with open("{0}_ref2.Acc".format(splID),'wt') as outhandle:
        outhandle.write(fofn)


if __name__ == "__main__":
    parser = ArgumentParser(description='Update references based on BLAST results')
    parser.add_argument('-BlastTable','--BlastTable', required=True, help='BLAST results')
    parser.add_argument('-refTable','--refTable', required=True, help='references selected based on Centrifuge report')
    parser.add_argument('-splID','--splID', required=True, help='Sample ID')

    opts, unknown_args = parser.parse_known_args()
    dict = BRtable(opts.BlastTable)
    f = fofnRef(opts.refTable, dict)
    writeFofn(opts.splID, f)
