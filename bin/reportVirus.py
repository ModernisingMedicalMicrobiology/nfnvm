#! /usr/bin/env python3
import sys
from Bio import SeqIO
import gzip
import os
import subprocess
import argparse
import csv


def dictC(inCov):
	dictCov={}

	covFile = open(inCov, 'r')
	covFile.readline()
	for line in covFile:
		line = line.rstrip()
		Ctemp = line.split("\t")
		accID = Ctemp[0]
		
		covText = "\t".join(( Ctemp[1], Ctemp[2], Ctemp[3] ))
		dictCov.setdefault(accID, covText)

	covFile.close()
	return dictCov

def dictM(inMap):
    dictMap={}

    mapFile = open(inMap, 'r')
    mapFile.readline()
    for line in mapFile:
        line = line.rstrip()
        Mtemp = line.split("\t")

        accID = Mtemp[0]
        mapText = "\t".join((Mtemp[1], Mtemp[4]))
        dictMap.setdefault(accID, mapText)

    mapFile.close()
    return dictMap

def dictFluTable(fluTable):
	dictFlu = {}

	fluTable_file = open(fluTable, 'r')
	for line in fluTable_file:
		line = line.rstrip()
		temp = line.split("\t")
		seqAcc = temp[0]
		subtype = temp[3]
		dictFlu.setdefault(seqAcc, subtype)

	fluTable_file.close()
	return dictFlu

def combineReport(inRef, dictCov, dictMap, dictFlu, splID):
	fofn=''
	headline = "\t".join(("SampleID", "Ref_accession", "Species_name", "Subtype", "Mean_depth", "Coverage(%) at 1 fold", "Coverage(%) at 10 fold", "Mapped_reads", "Total_mapped_bases"))
	fofn+='{0}\n'.format(headline)

	refFile = open(inRef, 'r')
	refFile.readline()
	for Rline in refFile:
		Rline = Rline.rstrip()
		temp = Rline.split("\t")

		RaccID = temp[2]
		Rname = temp[1]

		try:
			Rcov = dictCov[RaccID]
			Rmap = dictMap[RaccID]
			Rsubtype = dictFlu[RaccID]
			fofn+='{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(splID, RaccID, Rname, Rsubtype, Rcov, Rmap)

		except KeyError:
			try: 
				Rcov = dictCov[RaccID]
				Rmap = dictMap[RaccID]
				fofn+='{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'.format(splID, RaccID, Rname, "Unknown", Rcov, Rmap)
			except KeyError:
				pass
			
	return fofn

def writeFofn(splID, fofn):
    with open("{0}.viralReport".format(splID),'wt') as outhandle:
        outhandle.write(fofn)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Produce report for identified viral species')
    parser.add_argument("-inCov", dest="inCov", required= True, help = "Coverage stats")
    parser.add_argument("-inMap", dest="inMap", required= True, help = "Mapping stats")
    parser.add_argument("-inRef", dest="inRef", required= True, help = "Reference sequences")
    parser.add_argument("-splID", dest="splID", required= True, help = "sample ID")
    parser.add_argument("-fluTable", dest="fluTable", required= True, help = "Table of flu sequences in BLAST database")    

    params= parser.parse_args()

    dictCov= dictC(params.inCov)
    dictMap= dictM(params.inMap)
    dictFlu = dictFluTable(params.fluTable)

    fofn= combineReport(params.inRef, dictCov, dictMap, dictFlu, params.splID)
    writeFofn(params.splID, fofn)
