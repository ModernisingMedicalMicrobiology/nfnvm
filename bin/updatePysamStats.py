#! /usr/bin/env python3
from __future__ import division
import argparse
from itertools import count
from collections import Counter
import os
from gzip import GzipFile


def calStats(inFile, splID):

    inf_out = open("{0}_samStatsUpdate.txt".format(splID), 'w')
    inf_out.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\n".format("chrom","pos","ref","coverage","matches","matches%","mismatches","mismatches%","indels","indels%","insertions","insertions%","dels","dels%","As","As%","Cs","Cs%","Gs","Gs%","Ts","Ts%"))
    
    inf_in = open(inFile, 'r')
    inf_in.readline()
    for line in inf_in:
        chrom = ""
        pos = 0
        ref = ""
        coverage = 0
        matches = 0
        matchesP = 0
        mismatches = 0
        mismatchesP = 0
        insertions = 0
        insertionsP = 0
        dels = 0
        delsP = 0
        As = 0
        AsP = 0
        Cs = 0
        CsP = 0
        Ts = 0
        TsP = 0
        Gs = 0
        GsP = 0
        indels = 0
        indelsP = 0

        line = line.rstrip()
        temp = line.split("\t")

        chrom = temp[0]
        pos = int(temp[1])
        ref = temp[2]
        coverage = int(temp[3])
        matches = int(temp[5])
        mismatches = int(temp[7])
        insertions = int(temp[11])
        dels = int(temp[9])
        As = int(temp[13])
        Cs = int(temp[15])
        Ts = int(temp[17])
        Gs = int(temp[19])
        indels = insertions + dels
        coverage_clean = coverage - dels
        
        if coverage==0 or coverage_clean==0:
            matchesP = 0
            mismatchesP = 0
            insertionsP = 0
            delsP = 0
            AsP = 0
            CsP = 0
            TsP = 0
            GsP = 0
            indelsP = 0

        else:
            # in pysamstats, (coverage = dels + A + C + T + G)
            # here, the proportion of A/C/G/T is calculated using coverage_clean (coverage_clean = coverage - dels)
            matchesP = matches/coverage_clean
            mismatchesP = mismatches/coverage_clean
            AsP = As/coverage_clean
            CsP = Cs/coverage_clean
            TsP = Ts/coverage_clean
            GsP = Gs/coverage_clean
            insertionsP = insertions/coverage
            delsP = dels/coverage
            indelsP = indels/coverage

        inf_out.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\n".format(chrom,pos,ref,coverage,matches,matchesP,mismatches,mismatchesP,indels,indelsP,insertions,insertionsP,dels,delsP,As,AsP,Cs,CsP,Gs,GsP,Ts,TsP))

    inf_in.close()
    inf_out.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Update mapping profile')
    
    parser.add_argument("-inFile", dest="inFile", required= True, help = "mapping profile from Pysamstas")
    parser.add_argument("-splID", dest="splID", required= True, help = "Sample ID")
    
    params = parser.parse_args()
    
    calStats(inFile = params.inFile, splID = params.splID)

