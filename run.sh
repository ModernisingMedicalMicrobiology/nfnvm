#!/usr/bin/env bash

# NanoSPC: identification and assemlby of pathogen genomes using direct-from-sample Nanopore metagenomic sequencing
# NanoSPC is a scalable, portable and cloud compatible pipeline for analyzing Nanopore sequencing data. 
# It can identify potentially pathogenic viruses and bacteria simultaneously to provide comprehensive characterization of individual samples. 
# It can also detect single nucleotide variants and assemble high quality complete consensus genome sequences.

#################### set parameters ####################
# rawDataDir     -  Specify directory in which contains input sequencing reads
# readpat        -	Specify format of the input sequencing reads (support .fastq.gz format) 
# dataDir        -	Specify directory in which output has to be created
# varcall        -  Specify variant calling mode, choose one from three available options: 1.medaka, 2.clair, 3.nanopolish(not recommended)
# rawDataDir2    - 	Specify directory in which contains the fast5 format signal data for the input sequencing reads (only need if use the nanopolish module for variant calling)
# demultiplex    - 	Specify barcode demultiplexing mode, choose one from four available options : 1.skip(default), 2.qcat, 3.normal, 4.strict
# worker_threads -  Specify number of threads to be used
# profile        -  Specify configuration strategy for pipeline (set "docker")


rawDataDir=".../inputDir/"
readpat="*.fastq.gz"
dataDir=".../outputDir"
varcall=medaka
rawDataDir2=""              
demultiplex=skip            
worker_threads=6          
profile="docker"            



#################### run pipeline ####################
nextflow run viralMetagenomicsDetection.nf -profile ${profile} --rawDataDir ${rawDataDir} --readpat ${readpat} \
--dataDir ${dataDir} --varcall ${varcall} --rawDataDir2 ${rawDataDir2} --demultiplex ${demultiplex} \
--worker_threads ${worker_threads} \
-with-trace -with-timeline
