println "  NanoSPC, identification and assemlby of pathogen genome using direct-from-samples Nanopore metagenomic sequencing  "
println "  Yifei Xu, Department of Medicine, University of Oxford  "
println "                                          "
println "******************************************"


/* 
 * Display help menu
*/
params.help = false
if(params.help) {
    log.info ''
    log.info 'NanoSPC, identification and assemlby of pathogen genome using direct-from-samples Nanopore metagenomic sequencing'
    log.info ''
    return
 }


/*
 * General configuration variables
*/ 
params.rawDataDir = ""
params.rawDataDir2 = ""
params.dataDir = ""
params.readpat = "*BC*.fastq.gz"
data_path = params.rawDataDir + params.readpat

params.worker_threads = 1
worker_threads = params.worker_threads

params.demultiplex = false

params.QC = true
QC = params.QC

params.Cent = true
Cent = params.Cent

params.CentHitlen = 16
CentHitlen = params.CentHitlen

params.selRef = true
selRef = params.selRef

params.mapping = true
mapping = params.mapping

params.mapQ = 50
mapQ = params.mapQ

params.BLASTdraft = true
BLASTdraft = params.BLASTdraft

params.mapping2 = true
mapping2 = params.mapping2

params.report = true
report = params.report

params.varcall = "medaka"
varcall = params.varcall

params.consensus = true
consensus = params.consensus

params.verifyConsensus = true
verifyConsensus = params.verifyConsensus

params.fluResist = true
fluResist = params.fluResist


inFqs = Channel
    .fromPath(data_path)
    //.map {file -> tuple(file.getName().replace(params.readpat.replace("*", ""), ""), file)}
	.map {file -> tuple(file.simpleName, file)}
    .ifEmpty{exit 1, "Fastq files could not be found: ${params.rawDataDir}"}
	//.println()


/*
 * Process: demultiplex reads
*/
if(params.demultiplex == "skip") {
    process demultiplex_skip {
	
        //publishDir "${params.dataDir}/demultiplex_Out", mode: "copy", pattern: "*.fastq.gz"
        tag {pair_id}
        
        input:
        set pair_id, file(inFastq) from inFqs

        output:
        set pair_id, file("${pair_id}.fastq.gz") into demultiplex_Out
        
        script:
        """
        echo "Skip reads demultiplex." > demultiplex.txt
        """
    }
}

if(params.demultiplex == "qcat"){
    process demultiplex_qcat {
        label 'nfnvm'
        
		publishDir "${params.dataDir}/demultiplex_Out", mode: "copy", pattern: "barcode*.fastq*"
        tag {pair_id}
        
        input:
        set pair_id, file(inFastq) from inFqs

        output:
        file("barcode*.fastq*") into demultiplex_Out1
		
        script:
        """
		echo "qcat reads demultiplex." > demultiplex.txt
		gunzip -f ${inFastq}
		$qcat/qcat -f ${pair_id}.fastq -b ./ --detect-middle --trim --quiet
		gzip ./barcode*
        """
    }
    demultiplex_Out = demultiplex_Out1
            .flatten()
            .map {file -> tuple(file.simpleName, file)}
}

if(params.demultiplex == "normal"){
    process demultiplex_normal {
        label 'nfnvm'
		
        publishDir "${params.dataDir}/demultiplex_Out", mode: "copy", pattern: "BC*.fastq*"
        tag {pair_id}
        
        input:
        set pair_id, file(inFastq) from inFqs

        output:
        file("BC*.fastq*") into demultiplex_Out1
        
        script:
        """
		echo "normal porechop reads demultiplex." > demultiplex.txt
        $porechop/porechop -i ${inFastq} -b ./ -t ${worker_threads} --barcode_threshold 60 --discard_middle --format fastq.gz > demultiplex.log
        """
    }
    demultiplex_Out = demultiplex_Out1
            .flatten()
            .map {file -> tuple(file.simpleName, file)}
}

if(params.demultiplex == "strict"){
    process demultiplex_strict {

        label 'nfnvm'

        publishDir "${params.dataDir}/demultiplex_Out", mode: "copy", pattern: "BC*.fastq*"
        tag {pair_id}
        
        input:
        set pair_id, file(inFastq) from inFqs

        output:
        file("BC*.fastq*") into demultiplex_Out1
        
        script:
        """
		echo "strict porechop reads demultiplex." > demultiplex.txt
        $porechop/porechop -i ${inFastq} -b ./ -t ${worker_threads} --barcode_threshold 60 --discard_middle --require_two_barcodes --format fastq.gz > demultiplex.log
        """
    }
    demultiplex_Out = demultiplex_Out1
            .flatten()
            .map {file -> tuple(file.simpleName, file)}
}

/* 
 * Split the inFiles channel into multiple channels
*/
demultiplex_Out.into { inFiles_NanoPlot; inFiles_Centrifuge; inFiles_Assembly}


/* 
 * Process: quality control
*/
if (QC == true)
process qualityControl {

    label 'nfnvm'

	publishDir "${params.dataDir}/qualityControl_Out", mode: "copy", pattern: "${pair_id}_*"
    tag {pair_id} 
 
    input:
    set pair_id, file(inFastq) from inFiles_NanoPlot

    output:
    set pair_id, file("${pair_id}_*") into QC_Plot_Out

    script:
    """
    ${NanoPlot}/NanoPlot --fastq ${inFastq} -p ${pair_id}_ --color blue -f tiff -t ${worker_threads}
    """
}


/* 
 * Process: taxonomic classification of sequencing reads with centrifuge
*/
if (Cent == true)
process taxonomicClassification {

    label 'nfnvm'

    publishDir "${params.dataDir}/taxonomicClassification_Out", mode: "copy", pattern: "${pair_id}_*.report"

    memory '10 G'
    
    tag {pair_id}
	
	//python3 formatCentResults.py -inFile ${pair_id}_Cent.results -splID ${pair_id}
 
    input:
    set pair_id, file(inFastq) from inFiles_Centrifuge

    output:
	set pair_id, file("${inFastq}"), file("${pair_id}_Cent.resultsFormat"), file("${pair_id}_Cent.report") into Cent_Out

    script:
    """
    $CENTRIFUGE/centrifuge -q -x $CENTRIFUGE_db -U ${inFastq} --report-file ${pair_id}_Cent.report -S ${pair_id}_Cent.results --threads ${worker_threads} --min-hitlen ${CentHitlen} -k 1
	formatCentResultsV3.py -inFile ${pair_id}_Cent.results -splID ${pair_id} -table $TaxIDTable
    """
}


/* 
 * Process: select references based on centrifuge report
*/
if (selRef == true)
process selReference {
    label 'nfnvm'

    publishDir "${params.dataDir}/selReference_Out", mode: "copy", pattern: "${pair_id}_*.html"
 
    tag {pair_id}
 
    memory '2 G'
 
    input:
    set pair_id, file(inFastq), file("${pair_id}_Cent.resultsFormat"), file("${pair_id}_Cent.report") from Cent_Out
    
    output:
    set pair_id, file("${inFastq}"), file("${pair_id}_ref.Acc"), file("${pair_id}_classkrona.html"), file("${pair_id}_classFlu.Report") into selRef_Out

    script:
    """
    cat ${pair_id}_Cent.resultsFormat | cut -f 1,3 > ${pair_id}_classResults.krona
    $KRONA/ktImportTaxonomy ${pair_id}_classResults.krona -o ${pair_id}_classkrona.html
    
    binFluSegment.py -fluTable $CENTRIFUGE_FluTable -Cent ${pair_id}_Cent.resultsFormat -splID ${pair_id}
    findRef.R ${pair_id}_classFlu.Report ${pair_id}
    """
}


/* 
 * Process: mapping reads to selected references
*/
if (mapping == true)
process mapping1 {

    label 'nfnvm'

	//publishDir "${params.dataDir}/mapping1_Out", mode: "copy", pattern: "${pair_id}_ref*"

    memory '2 G'

    tag {pair_id}

    input:
    set pair_id, file(inFastq), file("${pair_id}_ref.Acc"),  file("${pair_id}_classkrona.html"), file("${pair_id}_classFlu.Report") from selRef_Out

    output:
    set pair_id, file("${inFastq}"), file("${pair_id}_ref.Acc"), file("${pair_id}_ref.fasta"), file("${pair_id}_sort.bam"), file("${pair_id}_sort.bam.bai") into mapping_OutA

    script:
	"""
    echo -e "KP406723.1\tHazara" >>${pair_id}_ref.Acc
	cat ${pair_id}_ref.Acc | cut -f 1 > ${pair_id}.refAccNumber    
	$SEQTK/seqtk subseq $CENTRIFUGE_Fasta ${pair_id}.refAccNumber > ${pair_id}_rawRef.fasta
	cat ${pair_id}_rawRef.fasta $HazaraControl > ${pair_id}_ref.fasta
	
    $MINIMAP2/minimap2 -ax map-ont ${pair_id}_ref.fasta ${inFastq} -t ${worker_threads} > ${pair_id}.sam
    samtools view -bq ${mapQ} ${pair_id}.sam --threads ${worker_threads} > ${pair_id}.bam
    samtools sort ${pair_id}.bam -o ${pair_id}_sort.bam --threads ${worker_threads}
    samtools index ${pair_id}_sort.bam -@ ${worker_threads}
    """
}


/* 
 * Process: refine reference selection through BLAST draft consensus sequence to viral sequence database
*/
if (BLASTdraft == true)
process refineReference {
    label 'nfnvm'

    //publishDir "${params.dataDir}/refineReference_Out", mode: "copy", pattern: "${pair_id}_*"
    
    memory '1 G'
    
    tag {pair_id}
 
    input:
    set pair_id, file(inFastq), file("${pair_id}_ref.Acc"), file("${pair_id}_ref.fasta"), file("${pair_id}_sort.bam"), file("${pair_id}_sort.bam.bai") from mapping_OutA
    
    output:
    set pair_id, file("${inFastq}"), file("${pair_id}_BLASTdraft.txt"), file("${pair_id}_ref2.Acc") into BLASTdraft_Out

    script:
    """
    pysamstats -f ${pair_id}_ref.fasta --type variation ${pair_id}_sort.bam --pad > ${pair_id}_samStats.txt
    updatePysamStats.py -inFile ${pair_id}_samStats.txt -splID ${pair_id}    
    voteConsensus.py -inFile ${pair_id}_samStatsUpdate.txt -splID ${pair_id} -depCov 0 -pctCall 0.01
    blastn -task blastn -query ${pair_id}_voteConsensus.fasta -db "$BLAST_db" -outfmt 6 -evalue 1e-10 -max_target_seqs 1 -num_threads ${worker_threads} | sort -k1,1 -k12,12nr -k11,11n | sort -u -k1,1 --merge > ${pair_id}_BLASTdraft.txt
    updateRef.py -BlastTable ${pair_id}_BLASTdraft.txt -refTable ${pair_id}_ref.Acc -splID ${pair_id}
    """
}


/* 
 * Process: mapping reads to refined references
*/
if (mapping2 == true)
process mapping2 {

    label 'nfnvm'

    publishDir "${params.dataDir}/mapping2_Out", mode: "copy", pattern: "${pair_id}_*"
   
    memory '2 G'
   
    tag {pair_id}

    input:
    set pair_id, file(inFastq), file("${pair_id}_BLASTdraft.txt"), file("${pair_id}_ref2.Acc") from BLASTdraft_Out

    output:
    //set pair_id, file("${pair_id}_ref2.fasta"), file("${pair_id}.mapReportA"), file("${pair_id}_*"), file("${pair_id}.mapping") into mapping2_OutB
    set pair_id, file("${pair_id}_*_cov.png"), file("${pair_id}.covReport"), file("${pair_id}_ref2.fasta"), file("${pair_id}_ref2.Acc"), file("${pair_id}.mapDepthReport"), file("${pair_id}_sort.bam"), file("${pair_id}_sort.bam.bai"), file("${pair_id}.mapStats") into mapping2_OutC

    script:
    """
    cat ${pair_id}_ref2.Acc | cut -f 3 > ${pair_id}.refAccNumber2
	$SEQTK/seqtk  subseq $BLAST_Fasta ${pair_id}.refAccNumber2 > ${pair_id}rawRef2.fasta
    seqkit rmdup -s -i ${pair_id}rawRef2.fasta -o ${pair_id}_ref2.fasta

    $MINIMAP2/minimap2 -ax map-ont ${pair_id}_ref2.fasta ${inFastq} -t ${worker_threads} > ${pair_id}.sam
	samtools view -bq ${mapQ} ${pair_id}.sam --threads ${worker_threads} > ${pair_id}_old.bam
	samtools sort ${pair_id}_old.bam -o ${pair_id}_oldSort.bam --threads ${worker_threads}
	samtools index ${pair_id}_oldSort.bam -@ ${worker_threads}
	filterBam.py -inFile ${pair_id}_oldSort.bam -splID ${pair_id}
	
    samtools sort ${pair_id}_new.bam -o ${pair_id}_sort.bam --threads ${worker_threads}
    samtools depth -aa -d 100000 ${pair_id}_sort.bam > ${pair_id}.mapDepth
    echo -e "ref\tposition\tcoverage" | cat - ${pair_id}.mapDepth > temp && mv temp ${pair_id}.mapDepthReport
    samtools flagstat ${pair_id}_sort.bam --threads ${worker_threads} > ${pair_id}.mapReportA
    samtools stats ${pair_id}_sort.bam --threads ${worker_threads} > ${pair_id}.mapReportB
    samtools index ${pair_id}_sort.bam -@ ${worker_threads}
    plotCoverage.R ${pair_id}.mapDepthReport ${pair_id}
    """
}


/* 
 * Process: report results
*/
if (report == true)
process report {
    label 'nfnvm'

    publishDir "${params.dataDir}/report_Out", mode: "copy", pattern: "${pair_id}*Report"
    tag {pair_id}

    input:
    set pair_id, file("${pair_id}_*_cov.png"), file("${pair_id}.covReport"), file("${pair_id}_ref2.fasta"), file("${pair_id}_ref2.Acc"), file("${pair_id}.mapDepthReport"), file("${pair_id}_sort.bam"), file("${pair_id}_sort.bam.bai"), file("${pair_id}.mapStats") from mapping2_OutC

    output:
    set pair_id, file("${pair_id}.viralReport"), file("${pair_id}.fluReport"), file("${pair_id}_ref2.fasta") into report_Out

    script:
    """
    reportVirus.py -inCov ${pair_id}.covReport -inMap ${pair_id}.mapStats -inRef ${pair_id}_ref2.Acc -splID ${pair_id} -fluTable $BLAST_FluTable
    reportFlu.R ${pair_id}.viralReport ${pair_id}
    """
}


/* 
 * Process: select genomes with >5 coverage for assembly
*/
process prepAssembly {

    label 'nfnvm'

    //publishDir "${params.dataDir}/prepAssembly_Out", mode: "copy", pattern: "*.fasta"
    tag {splID}

    input:
	set splID, file(inreport), file(flureport), file(inref) from report_Out

    output:
    file("${splID}.REF*.fasta") into prepAssembly_Out
  
  script:
    """
    selAssemblyRef.R ${inreport} ${splID}
    $SEQTK/seqtk subseq ${inref} ${splID}.selRef > ${splID}_refAssembly.fasta
    splitRef.py -oriFile ${splID}_refAssembly.fasta -splID ${splID}
    """
}

prepRef = prepAssembly_Out
			.flatten()
			.map {file -> tuple(file.simpleName, file)}

inAssembly = inFiles_Assembly
	.combine(prepRef, by:0)


/* 
 * Process: prepare input files for variant calling with clair
*/
process prepVarcall {

    label 'nfnvm'

	//publishDir "${params.dataDir}/prepVarcall_Out", mode: "copy", pattern: "*"
    tag {inref.getBaseName()}

    input:
    set splID, file(infastq), file(inref) from inAssembly

    output:
    set val("${inref.getBaseName()}"), file("${inref.getBaseName()}.fasta"), file("${inref.getBaseName()}_sort.bam"), file("${inref.getBaseName()}_sort.bam.bai"), file("${inref.getBaseName()}_mapped.fastq") into prepVarcall_Out

	script:
    """
	$MINIMAP2/minimap2 -ax map-ont ${inref} ${infastq} -t ${worker_threads} > ${inref.getBaseName()}.sam
	samtools view -b ${inref.getBaseName()}.sam --threads ${worker_threads} > ${inref.getBaseName()}.bam
	samtools sort ${inref.getBaseName()}.bam -o ${inref.getBaseName()}_sort.bam --threads ${worker_threads}
	samtools index ${inref.getBaseName()}_sort.bam -@ ${worker_threads}
	bedtools bamtofastq -i ${inref.getBaseName()}_sort.bam -fq ${inref.getBaseName()}_mapped.fastq
	"""
}


if(varcall== "clair"){
process varcall_clair {

    label 'clair'
    
    queueSize = 3
	errorStrategy 'ignore'
	
	publishDir "${params.dataDir}/varcall_Out", mode: "copy", pattern: "*.vcf"
	tag {splID}
	
	input:
	set splID, file(inref), file("${splID}_sort.bam"), file("${splID}_sort.bam.bai"), file("${splID}_mapped.fastq") from prepVarcall_Out
	
	output:
	set splID, file("${splID}.vcf"), file(inref), file("${splID}_sort.bam"), file("${splID}_mapped.fastq") into varcall_Out
	
	script:
    """
    cat <<- EOF >> chop.py
	import sys

	temp = sys.argv[1].replace(".fasta", "").split("_")[1]
	print(temp)

	EOF
	
	CONTIG=\$(python3 chop.py ${inref})
	echo \${CONTIG} > printSampleID.txt
	
	samtools faidx ${inref}
	
	$CLAIR callVarBam --chkpnt_fn $ClairModel/model --ref_fn ${inref} --bam_fn ${splID}_sort.bam --sampleName ${splID} --threads ${worker_threads} --call_fn ${splID}.vcf --threshold 0.2 --qual 750 --ctgName \${CONTIG}
	"""
}
}


if(varcall== "medaka"){
process varcall_medaka {

    label 'medaka'

    queueSize = 3
	
	publishDir "${params.dataDir}/varcall_Out", mode: "copy", pattern: "*.vcf"
	tag {splID}
	
	input:
	set splID, file(inref), file("${splID}_sort.bam"), file("${splID}_sort.bam.bai"), file("${splID}_mapped.fastq") from prepVarcall_Out
	
	output:
	set splID, file("${splID}.vcf"), file(inref), file("${splID}_sort.bam"), file("${splID}_mapped.fastq") into varcall_Out
	
	script:
    """
	medaka_version_report > version.txt
    medaka_variant_local.sh -i ${splID}_sort.bam -f ${inref} -o ./ -t ${worker_threads}
    mv round_1.vcf ${splID}.vcf
	"""
}
}


if(varcall== "nanopolish"){
process varcall_nanopolish {

    label 'nanopolish'

    queueSize = 3
	
	publishDir "${params.dataDir}/varcall_Out", mode: "copy", pattern: "*.vcf"
	tag {splID}
	
	input:
	set splID, file(inref), file("${splID}_sort.bam"), file("${splID}_sort.bam.bai"), file("${splID}_mapped.fastq") from prepVarcall_Out
	
	output:
	set splID, file("${splID}.vcf"), file(inref), file("${splID}_sort.bam"), file("${splID}_mapped.fastq") into varcall_Out
	
	script:
    """
	$Nanopolish/nanopolish index -d ${params.rawDataDir2} ${splID}_mapped.fastq
    $Nanopolish/nanopolish variants --snps --ploidy 1 --calculate-all-support -o ${splID}.vcf -r ${splID}_mapped.fastq -b ${splID}_sort.bam -g ${inref} --min-candidate-frequency 0.1 -d 10 -t ${worker_threads}
	"""
}
}

if (consensus == true)
process consensus {
    label 'nfnvm'

	//publishDir "${params.dataDir}/consensus_Out", mode: "copy", pattern: "*"
	tag {splID}
	
	input:
	set splID, file("${splID}.vcf"), file(inref), file("${splID}_sort.bam"), file("${splID}_mapped.fastq") from varcall_Out
	
	output:
	set splID, file("${splID}_con.fasta"), file("${splID}.report"), file("${splID}_mapped.fastq") into consensus_Out
	
	script:
    """
	margin_cons_v2.py ${inref} ${splID}.vcf ${splID}_sort.bam ${varcall} 2>${splID}.report >${splID}_con.fasta
	"""
}

	
/* 
 * Process: verify consensus sequence
*/
if (verifyConsensus == true)
process verifyConsensus {  

    label 'nfnvm'

    publishDir "${params.dataDir}/verifyConsensus_Out", mode: "copy", pattern: "*"
    tag {splID} 

    input:
	set splID, file("${splID}_con.fasta"), file("${splID}.report"), file("${splID}_mapped.fastq") from consensus_Out

    output:
    set splID, file("${splID}_verifyCon.fasta") into verifyConsensus_Out

  script:
    """
    $MINIMAP2/minimap2 -ax map-ont ${splID}_con.fasta ${splID}_mapped.fastq -t ${worker_threads} > ${splID}_verify.sam
    samtools view -bq 50 ${splID}_verify.sam --threads ${worker_threads} > ${splID}_verify.bam
    samtools sort ${splID}_verify.bam -o ${splID}_verify_sort.bam --threads ${worker_threads}
    samtools index ${splID}_verify_sort.bam -@ ${worker_threads}

    pysamstats -f ${splID}_con.fasta --type variation ${splID}_verify_sort.bam --pad > ${splID}_samStats.txt
    updatePysamStats.py -inFile ${splID}_samStats.txt -splID ${splID}
    verifyCon.py -inFile ${splID}_samStatsUpdate.txt -splID ${splID} -depCov -1 -pctCall 0.7
    """
}


/* 
 * Process: identify drug-resistant mutations for influenza
*/
if (fluResist == true)
process fluResist {

    label 'nfnvm'
	
    publishDir "${params.dataDir}/fluResist_Out", mode: "copy", pattern: "*fluResistance"
    tag {splID} 

    input:
	set splID, file("${splID}_verifyCon.fasta") from verifyConsensus_Out

    output:
    file("*") into fluResist_Out

  script:
    """
	blastx -task blastx -query ${splID}_verifyCon.fasta -db "${fluProteinRef}" -outfmt 5 -evalue 1e-10 -max_target_seqs 1 >${splID}.BLASTreport
	fluALN.py -blastFile ${splID}.BLASTreport -splID ${splID}
	fluResistance.py -fluResistTable $fluResistTable -seqFile ${splID}_protein.fasta  -splID ${splID}
    """
}

process outputGuide{

    label 'nfnvm'

    publishDir "${params.dataDir}/" , mode: "copy", pattern: "*.txt"

    output:
    file("*.txt")

    script:
    """
    wget  https://gitlab.com/ModernisingMedicalMicrobiology/nfnvm/-/raw/master/ResultGuide.txt

    """
}


/* 
 * Display information about the completed run
 */
workflow.onComplete {
    log.info "                                          "
    log.info "                                          "
    log.info "******************************************"
    log.info "Nextflow Version: $workflow.nextflow.version"
    log.info "Command line: $workflow.commandLine"
    log.info "Container:    $workflow.container"
    log.info "Duration: $workflow.duration"
    log.info "Output data directory: $params.dataDir"
}
