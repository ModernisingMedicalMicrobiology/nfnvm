# NanoSPC

Nanopore metagenomic sequencing has the potential to become a point-of-care test for infectious diseases in clinical and public health settings, providing rapid diagnosis of infection, guide individual patient management and treatment strategies including antimicrobial prescribing and stewardship, and informing infection prevention and control practices.

NanoSPC is a scalable, portable and cloud compatible pipeline for analyzing Nanopore sequencing data. It can identify potentially pathogenic viruses and bacteria simultaneously to provide comprehensive characterization of individual samples. It can also detect single nucleotide variants and assemble high quality complete consensus genome sequences.

NanoSPC provides cloud-based and standalone applications. Visit [NanoSPC website](https://nanospc.mmmoxford.uk/) for more information and run NanoSPC on cloud. This document describes how to run NanoSPC standalone application. 



##  Part 1 Installation

##  1.1 Prerequisites

This standalone application implements Nextflow manager with Docker images that contains all the software dependencies. Please install nextflow and docker.

Install nextflow if not exists from [Nextflow Documentation](https://www.nextflow.io/docs/latest/getstarted.html)

Install docker if not exists from [Docker Documentation](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

##  1.2 Get NanoSPC code from GitLab

    #git clone https://gitlab.com/ModernisingMedicalMicrobiology/nfnvm

##  1.3 Get NanoSPC Docker images

We build Docker images that wrap the entire pipeline into a single environment. Docker images are available to pull from Docker hub.

Docker image for NanoSPC main pipeline

    #docker pull oxfordmmm/nfnvm:v0.6.0

NanoSPC enables three modes, i.e. medaka, clair, nanopolish, for variant calling and genome assembly. We provide Docker images per mode.
Choose one of the following images (medaka, clair, nanopolish):

    #docker pull oxfordmmm/medaka:v0.11.5

    #docker pull oxfordmmm/clair:v2.0.7

    #docker pull oxfordmmm/nanopolish:v0.11.0

If you like to run with Singularity container, please use following command to convert Docker image into Singularity image and update nextflow configuration.

    #singularity build nfnvm-v0.6.0.img docker://oxfordmmm/nfnvm:v0.6.0

##  1.4 Get reference database

Download the reference sequence database used in the pipeline from [reference database](https://nanospc.mmmoxford.uk/e95559d5-27bc-472e-821d-035fed7ff030/reference.tar.gz) (size: 6.9G). 

After download, put reference database in the directory of nfnvm: ".../nfnvm/nanospc/referenceDatabase".

Next adjust the path of reference database in the nextflow configuration file. Open ".../nfnvm/nextflow.config" and find the docker profile (line 102 to 112), replace “/home/ubuntu/Code/nfnvm/referenceDatabase” with the path of reference database in your machine. For example, we have reference database in local directory "/home/yifei/Projects/nfnvm/nanospc/referenceDatabase", replace “/home/ubuntu/Code/nfnvm/referenceDatabase” with "/home/yifei/Projects/nfnvm/nanospc/referenceDatabase".

NanoSPC uses two reference database in the analysis: 1) the centrifuge reference database (p_compressed+h+v) comprises 20,174 complete bacterial, archaeal, viral, and human genomes corresponding to 11,539 taxonomic IDs; 2) the viral reference database comprises >86,000 complete genomes of viral pathogens downloaded from NIAID Virus Pathogen Database and Analysis Resource (ViPR) in March 2020.




##  Part 2 Running a test dataset

We use a test dataset to give a guide on how to run the pipeline.

##  2.1 Get test data

Download a test dataset to try the pipeline from [test data](https://files.mmmoxford.uk/f/594443dfb83946669402/).

This is a fastq file (.fastq.gz format) containing metagenomic sequencing reads generated using Nanopore platform. 

##  2.2 Set parameters and run the pipeline

Open ".../nfnvm/run.sh" and set the following parameters:

    -rawDataDir         Specify directory in which contains input sequencing reads 
    -readpat            Specify format of the input sequencing reads (support .fastq.gz format) 
    -dataDir            Specify directory in which output has to be created
    -varcall            Specify variant calling mode, choose one from three available options: 1.medaka, 2.clair, 3.nanopolish(not recommended)
    -rawDataDir2        Specify directory in which contains the fast5 format signal data for the input sequencing reads (only need if use the nanopolish module for variant calling)
    -demultiplex=skip   Specify barcode demultiplexing mode, choose one from four available options : 1.skip(default), 2.qcat, 3.normal, 4.strict
    -worker_threads     Specify number of threads to be used
	-profile            Specify configuration strategy for pipeline (set "docker")

NanoSPC enables three variant calling modes, namely Medaka, Clair, and Nanopolish. In Medaka and Clair Mode, NanoSPC applies Medaka (ONT) and clair to detect single nucleotide variants. In Nanopolish Mode, Nanopolish mode takes both sequencing reads of the fastq format and raw signal data of the fast5 format as inputs, and applies Nanopolish to detect single nucleotide variants. This mode is computationally intensive and time consuming, thus not recommended.

NanoSPC enables three demultiplexing modes, namely default porechop, strict porechop, and qcat. The default porechop mode requires a barcode sequence to be present at either end of each read using porechop, thus maximizing the number of classified reads for downstream analysis. The strict porechop mode requires the same barcode sequence to be present at both ends of each read, minimizing the number of misclassified reads. The qcat mode employs the demultiplexer offered by ONT and works similar to the default porechop mode.

In this test, we use the medaka variant calling mode and skip barcode demultiplexing. We set parameters as follows,

    rawDataDir="/home/projects/data/input/"
    readpat="*fastq.gz"
    dataDir="/home/projects/data/output"
    varcall=medaka
    rawDataDir2=""
    demultiplex=skip
    worker_threads=5
	profile="docker"

then run .../nfnvm/run.sh
	
	#./run.sh

Upon completion, you will see messages like this

![testFig1](tutorial/testFig1.png)

##  2.3 Guide to result files

The result files generated for this test dataset is shown below.

![testFig2](tutorial/testFig2.png)

The result files are arranged into multiple folders, and each comprises files generated from the same process in the pipeline. The following folders and files present the major results.

    qualityControl_Out:
        SampleName_NanoStats.txt  -  statistical summary of data quality
        SampleName_NanoPlot-report.html  -  html summary file of data quality

    taxonomicClassification_Out: 
        SampleName_Cent.report  -  summary report of taxonomic classification via centrifuge 

    selReference_Out: 
        SampleName_classkrona.html  -  html visualization of metagenomic classifications
    
    mapping2_Out: 
        SampleName_ref2.fasta  -  reference sequences determined for each viral species 
        SampleName_sort.bam  -  bam file generated by mapping reads against viral references 
    
    report_Out: 
        SampleName.viralReport  -  summary report of non-influenza viruses detected from the data  
        SampleName.fluReport  -  summary report of influenza viruses detected from the data
    
    varcall_Out: 
        SampleName.REF_accession.vcf  -  vcf file listing called variants
    
    verifyConsensus_Out: 
        SampleName.REF_accession_verifyCon.fasta  -  consensus sequence
		
	fluResist_Out: 
		SampleName.REF_accession.fluResistance  -  influenza drug-resistant mutations

##  2.4 Result for the test data

We show some results for this test data.

A) The statistical summary of data quality:

![qualityControl](tutorial/qualityControl.png)

Plot showing read lengths against average read quality.

![A5_LengthvsQualityScatterPlot_dot](tutorial/A5_LengthvsQualityScatterPlot_dot.png)

B) Metagenomic classifications of reads:

Bacterial and viral reads accounted for 88% and 5% of the total sequencing reads. Among the viral reads, 67% and 33% are identified as human metapneumovirus (hmpv) and human parainfluenza virus type 3 (hpiv-3) reads.

![classification1](tutorial/classification1.png)

![classification2](tutorial/classification2.png)

C) Identification of viral species:

![species](tutorial/species.png)

D) Genome mapping coverage:

14,821 hmpv reads cover the complete genome at mean coverage of 886, and 7,323 hpiv-3 reads cover the complete genome at mean coverage of 370.

![mapping1](tutorial/mapping1.png)

![mapping2](tutorial/mapping2.png)

E) Consensus sequences:

Two consensus sequences, "test.REF_KY474539_verifyCon.fasta"(hmpv) and "test.REF_KY933414_verifyCon.fasta"(hpiv-3), are produced.



##  About us

Yifei Xu, Bioinformatician, Nuffield Department of Medicine, University of Oxford

Fan Yang, Lead Software Engineer, Nuffield Department of Medicine, University of Oxford

Denis Volk, Software Engineer, Nuffield Department of Medicine, University of Oxford
